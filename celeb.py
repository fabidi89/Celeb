from collections import OrderedDict
from bokeh.sampledata import us_states, us_counties, unemployment
from bokeh.plotting import figure, show, output_file, ColumnDataSource
from bokeh.models import HoverTool
from bokeh.plotting import *
from bokeh.palettes import *
from bokeh.models import *
import pandas as p
import numpy as np
import colorsys
import json


def get_image_links():

	ent = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Dec02/Celebrity_Database__dec_02_ent.csv')
	ent.columns
	clean = ent[['Name', 'Category','Company','Food and beverage category','Product line','Ethnicity','Age','Unnamed: 7']].dropna(how='any')
#clean = ent[['Name','Company','Food and beverage category','Product line','Unnamed: 7','Unnamed: 8']].dropna(how='any')
#clean.rename(columns={'Unnamed: 7':'Ethnicity'},inplace=True)
	clean.rename(columns={'Unnamed: 7':'link'},inplace=True)
	clean.rename(columns={'Unnamed: 8':'Age'},inplace=True)
	clean['tag'] = 'ent'

	ath = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Dec02/Celebrity_Database__dec_02_ath.csv')
	clean_ath = ath[['Name', 'Category', 'Company', 'Food and beverage category','Product line','Code','Age','Unnamed: 7']].dropna(how='any')
	clean_ath.rename(columns={'Code':'Ethnicity'},inplace=True)
	clean_ath.rename(columns={'Unnamed: 7':'link'},inplace=True)
	clean_ath['tag'] = 'ath'
	
	oth = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Dec02/Celebrity_Database__dec_02_oth.csv')
	clean_oth = oth[['Name', 'Category', 'Company','Food and beverage category','Product line','Code','Age','Unnamed: 7']].dropna(how='any')
	clean_oth.rename(columns={'Code':'Ethnicity'},inplace=True)
	clean_oth.rename(columns={'Unnamed: 7':'link'},inplace=True)
	clean_oth['tag'] = 'oth'

	fnv = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Dec02/Celebrity_Database__dec_02_fnv.csv')
	clean_fnv = fnv[['Name', 'Category', 'Company','Food and beverage category','Product line','Code','Age','Unnamed: 7']].dropna(how='any')
	clean_fnv.rename(columns={'Code':'Ethnicity'},inplace=True)
	clean_fnv.rename(columns={'Unnamed: 7':'link'},inplace=True)
	clean_fnv['tag'] = 'fnv'
	
	milk = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Dec02/Celebrity_Database__dec_02_milk.csv')
	clean_milk = milk[['#NAME?', 'Category', 'Company','Food and beverage category','Product line','Code ','Age','Unnamed: 7']].dropna(how='any')
	clean_milk.rename(columns={'Code ':'Ethnicity'},inplace=True)
	clean_milk['tag'] = 'milk'
	clean_milk.rename(columns={'#NAME?':'Name'},inplace=True)
	clean_milk.rename(columns={'Unnamed: 7':'link'},inplace=True)
	clean_ath.rename(columns={'Code':'Ethnicity'},inplace=True)

	clean['link'] = clean['link'].convert_objects(convert_numeric=True).dropna(how='any')
	clean.dropna(how='any',inplace=True)
	clean['link'].astype('int')

	clean_fnv['link'] = clean_fnv['link'].convert_objects(convert_numeric=True).dropna(how='any')
	clean_fnv.dropna(how='any',inplace=True)
	clean_fnv['link'].astype(int)

	clean_oth['link'] = clean_oth['link'].convert_objects(convert_numeric=True).dropna(how='any')
	clean_oth.dropna(how='any',inplace=True)
	clean_oth['link'].astype(int)

	clean_ath['link'] = clean_ath['link'].convert_objects(convert_numeric=True).dropna(how='any')
	clean_ath.dropna(how='any',inplace=True)
	clean_ath['link'].astype(int)

	clean_milk['link'] = clean_milk['link'].convert_objects(convert_numeric=True).dropna(how='any')
	clean_milk.dropna(how='any',inplace=True)
	clean_milk['link'].astype('int')

	
	flat = p.concat([clean,clean_ath,clean_oth,clean_fnv,clean_milk])
	flat.dropna(how='any')
	flat['Name'].describe() # get all unique names
	
	intv = np.vectorize(int)
	tlen = len(flat)
	flat.index = intv(np.linspace(1,tlen,tlen))
	
	flat['class'] = '0'
	flat[flat['Food and beverage category'].isin(['QSR','Confection or Savory Snack or Cereal'])]
	flat.loc[flat['Food and beverage category'].isin(['Low-cal & no-cal beverages','Low cal and no cal beverages','Low-cal & no-cal beverage','Dairy','Dairy ','water','Fruits and Vegetables','Fruit and Vegetables','Milk']),'class'] = 'p'
	flat.loc[flat['Food and beverage category'].isin(['SSB','QSR','Confection or Savory snack or Cereal','Confection or Savory Snack or Cereal','Cereal']),'class'] = 'n'
	flat_sub = flat[['Name','tag','class','link']]
	#flat_sub[np.isnan(flat_sub['link'])]
	for i in range(1,len(flat)):
		print i , flat_sub.loc[i]['link'] , flat_sub.loc[i]['tag']
	flat_sub['link'] = flat_sub['link'].astype(int)
	
	flat_sub.to_csv('clean_links.csv')
	with open("text", "w") as outfile:
		json.dump(flat_sub.to_json(),outfile)

	globals()['myvar'] = flat_sub


def get_json(myvar):
		df = p.DataFrame({
		"time" : myvar['Name'].values,
    	"temp" : myvar['link'].values
		})

		d = [ 
    		dict([
        	(colname, row[i]) 
        	for i,colname in enumerate(df.columns)
    		])
    	for row in df.values
		]
		return json.dumps(d)

def get_dict(myvar):
		df = p.DataFrame({
		"Name" : myvar['Name'].values,
    	"link" : myvar['link'].values
		})

		d = [ 
    		dict([
        	(colname, row[i]) 
        	for i,colname in enumerate(df.columns)
    		])
    	for row in df.values
		]
		return d				 


ent = p.read_csv('/Users/srijithrajamohan/Documents/kraak/celeb/Entertainment-table_1.csv')

# Name and Q-scores
qscore = ent[['Name','Q-scores']] 

#sort set by Q-scores
ent[['Name','Q-scores']].dropna(how='any').sort('Q-scores')

#Name and age
age = ent[['Name','Unnamed: 8']]
#replace Unnamed column with Age
age.rename(columns={'Unnamed: 8':'Age'},inplace=True)


ent = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Celebrity/Entertainment-Table_1.csv')
ent = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Updated/CelebrityDatabase_Latest_Nov4.xlsx-Entertainment.csv')
ent = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Latest/Dat_ent.csv')
ent = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Dec02/Celebrity_Database__dec_02_ent.csv')
ent.columns
clean = ent[['Name', 'Category','Company','Food and beverage category','Product line','Ethnicity','Age']].dropna(how='any')
#clean = ent[['Name','Company','Food and beverage category','Product line','Unnamed: 7','Unnamed: 8']].dropna(how='any')
#clean.rename(columns={'Unnamed: 7':'Ethnicity'},inplace=True)
clean.rename(columns={'Unnamed: 8':'Age'},inplace=True)
clean['tag'] = 'ent'


ath = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Celebrity/Athlete-Table_1.csv')
ath = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Updated/CelebrityDatabase_Latest_Nov4.xlsx-Athlete.csv')
ath = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Latest/Dat_Ath.csv')
ath = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Dec02/Celebrity_Database__dec_02_ath.csv')
ath.columns
clean_ath = ath[['Name', 'Category', 'Company', 'Food and beverage category','Product line','Code','Age']].dropna(how='any')
clean_ath.rename(columns={'Code':'Ethnicity'},inplace=True)
clean_ath['tag'] = 'ath'

oth = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Celebrity/Other-Table_1.csv')
oth = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Updated/CelebrityDatabase_Latest_Nov4.xlsx-Other.csv')
oth = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Latest/Dat_Oth.csv')
oth = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Dec02/Celebrity_Database__dec_02_oth.csv')
clean_oth = oth[['Name', 'Category', 'Company','Food and beverage category','Product line','Code','Age']].dropna(how='any')
clean_oth.rename(columns={'Code':'Ethnicity'},inplace=True)
clean_oth['tag'] = 'oth'

fnv = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Celebrity/FNV_celebs-Table_1.csv')
fnv = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Updated/CelebrityDatabase_Latest_Nov4.xlsx-FNVcelebs.csv')
fnv = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Latest/Dat_FNV.csv')
fnv = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Dec02/Celebrity_Database__dec_02_fnv.csv')
clean_fnv = fnv[['Name', 'Category', 'Company','Food and beverage category','Product line','Code','Age']].dropna(how='any')
clean_fnv.rename(columns={'Code':'Ethnicity'},inplace=True)
clean_fnv['tag'] = 'fnv'

milk = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Celebrity/Got_milk-Table_1.csv')
milk = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Updated/CelebrityDatabase_Latest_Nov4.xlsx-GotMilk.csv')
milk = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Latest/Dat_Milk.csv')
milk = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Dec02/Celebrity_Database__dec_02_milk.csv')
clean_milk = milk[['#NAME?', 'Category', 'Company','Food and beverage category','Product line','Code ','Age']].dropna(how='any')
clean_milk.rename(columns={'Code ':'Ethnicity'},inplace=True)
clean_milk['tag'] = 'milk'
clean_milk.rename(columns={'#NAME?':'Name'},inplace=True)
clean_ath.rename(columns={'Code':'Ethnicity'},inplace=True)

flat = p.concat([clean,clean_ath,clean_oth,clean_fnv,clean_milk])
flat['Name'].describe() # get all unique names
flat.to_csv('clean.csv')

intv = np.vectorize(int)
tlen = len(flat)
flat.index = intv(np.linspace(1,tlen,tlen))
flat.to_csv('clean_unique.csv')

#flat.ix[12].Age = '34'
#flat.ix[189].Age =  '40'
flat.ix[219].Age = '40'
flat.Age = flat.Age.convert_objects(convert_numeric=True)

flat['class'] = '0'
flat[flat['Food and beverage category'].isin(['QSR','Confection or Savory Snack or Cereal'])]
flat.loc[flat['Food and beverage category'].isin(['Low-cal & no-cal beverages','Low cal and no cal beverages','Low-cal & no-cal beverage','Dairy','Dairy ','water','Fruits and Vegetables','Fruit and Vegetables','Milk']),'class'] = 'p'
flat.loc[flat['Food and beverage category'].isin(['SSB','QSR','Confection or Savory snack or Cereal','Confection or Savory Snack or Cereal','Cereal']),'class'] = 'n'
flat.to_csv('clean_unique.csv')

flat[flat['Name'].duplicated() == True]

mdat = p.DataFrame([])
multiple_endorsements = flat[flat['Name'].duplicated(take_last=True)]['Name'].drop_duplicates()
m = multiple_endorsements
for i in range(0,m.count()):
 temp = flat[p.Index(flat['Name']).get_loc(m[m.index[i]]) == True]
 print temp
 mdat = mdat.append(temp)

print mdat[['Name','class']]

for i in flat.columns:
  print i
  print flat[p.isnull(flat[i]) == True]

get_image_links() 


positive = myvar[myvar['class'] == 'p']
pjson = get_json(positive)
negative = myvar[myvar['class'] == 'n']
njson = get_json(negative)

a = get_dict(positive)
b = get_dict(negative)
final = { "Positive" : a , "Negative" : b }
with open("my.json","w") as f:
    json.dump(final,f)








